<?php


namespace App\Entity;

class Article
{
  public $id;
  public $title;
  public $body;

  public function fromSQL(array $sql) {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->body = $sql["body"];
  }
}