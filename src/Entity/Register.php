<?php


namespace App\Entity;


class Register
{
  public $id;
  public $pseudo;
  public $email;
  public $password;

  public function fromSQL(array $sql) {
    $this->id = $sql["id"];
    $this->pseudo = $sql["pseudo"];
    $this->email = $sql["email"];
    $this->password = $sql["password"];
  }
}