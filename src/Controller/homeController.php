<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;


class homeController extends AbstractController
{
  /**
   * @Route("/", name="home")
   */

  public function index(ArticleRepository $repo)
  {
    $articles = $repo->getAll();

    return $this->render("home.html.twig", [
      'articles' => $articles
    ]);
  }
}
