<?php


namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class articleCreateController extends AbstractController
{
  /**
   * @Route("/articleCreate", name="articleCreate")
   * @Security("has_role('ROLE_ADMIN')")
   */

  public function article(Request $request, ArticleRepository $repo)
  {
    $article = new Article();

    $form = $this->createFormBuilder($article)
            ->add('title', TextType::class)
            ->add('body', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Créer un nouvel article'))
            ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $article = $form->getData();
      $repo->add($article);
    return $this->redirectToRoute("home");
      
    }

    return $this->render("articleCreate.html.twig", [
      'form' => $form->createView(),
    ]);
  }
}
