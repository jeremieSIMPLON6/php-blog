<?php


namespace App\Controller;


use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Entity\Register;
use App\Repository\RegisterRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class registerController extends AbstractController
{
  /**
   * @Route("/register", name="register")
   */

  public function register(Request $request, RegisterRepository $repo)
  {
    $register = new Register();

    $form = $this->createFormBuilder($register)
      ->add('pseudo', TextType::class)
      ->add('email', EmailType::class)
      ->add('password', PasswordType::class)
      ->add('save', SubmitType::class, array('label' => 'Enregistrer-vous'))
      ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $register = $form->getData();
      $repo->add($register);
    }
    $articles = $repo->getAll();

    return $this->render("register.html.twig", [
      'form' => $form->createView(),
      'articles' => $articles
    ]);
  }
}
