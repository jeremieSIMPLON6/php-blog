<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class articleReadController extends Controller
{
    /**
     * @Route("/articleRead/{id}", name="articleRead")
     */

    public function index(int $id, ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($repo->update($form->getData())) {
                return $this->redirectToRoute("home");
            }
        }

        return $this->render('articleRead.html.twig', [
            'form' => $form->createView(),
            'article' => $article,
        ]);
    }
    /**
     * @Route("/articleRemove/{id}", name="articleRemove")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function remove(int $id, ArticleRepository $repo)
    {
        $repo->delete($id);

        return $this->redirectToRoute("home");

    }
}
