<?php

namespace App\Repository;

use App\Entity\Register;

class RegisterRepository
{
  public function getAll() : array
  {

    $registers = [];
    try {
      $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
           
      $query = $cnx->prepare("SELECT * FROM register");
      $query->execute();

      foreach ($query->fetchAll() as $row) {
        $register = new Register();
        $register->fromSQL($row);
        $registers[] = $register;
      }

    } catch (\PDOException $e) { 
      dump($e);
    }
    return $registers;
  }

  public function add(Register $register)
  {
      try {
          $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

          $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

          $query = $cnx->prepare("INSERT INTO register (pseudo, email, password) VALUES (:pseudo, :email, :password)");
          
          $query->bindValue(":pseudo", $register->pseudo);
          $query->bindValue(":email", $register->email);
          $query->bindValue(":password", $register->password);

          $query->execute();

          $register->id = intval($cnx->lastInsertId());

      } catch (\PDOException $e) {
          dump($e);
      }
  }
}